/*
Copyright (c) 2008, Max Vilimpoc, http://vilimpoc.org/
Copyright (c) 2022, Johannes Lode, https://mtronig.de/

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

 * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the following
      disclaimer.
 * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials
      provided with the distribution.
 * Neither the name of the author nor the names of its
      contributors may be used to endorse or promote products
      derived from this software without specific prior written
      permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _GNU_SOURCE
#include <getopt.h>
#include <termios.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>

#include <time.h>
#include <sys/time.h>
#include <limits.h>
#include <errno.h>

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#include <pthread.h>

/* Our infinite loop controller. */
static bool keepRunning = true;

typedef enum
{
  CONST_DEVNAMESTR    =   256, /* i.e. /dev/ttyUSBnnnnnnnnnnnnnnnnn */
  CONST_CONFIGSTR     =   16,  /* i.e. 115200_8N1                   */
  CONST_PATTERNSTR    =   4096,
} STRVAR_CONSTANTS;

void Usage(const char * const progName)
{
  printf("\n");
  printf("Usage: %s [-d /dev/ttySn] [-c 115200_8N1] [-n 0] [-i 1.0] [-t 0.001] [-p testpattern] [-r rxpattern]\n", progName);
  printf("\n");
  printf("  [*]  --device    or  -d       specifies the serial device\n");
  printf("  [*]  --config    or  -c       specifies the device configuration\n");
  printf("       --interval  or  -i       specifies the send interval for TX pattern [s]\n");
  printf("       --timeout   or  -t       specifies the timeout for RX bytes [s] (default is 10.5 bytes)\n");
  printf("       --count     or  -n       limit the number of pings\n");
  printf("       --printtx   or  -P       print TX pattern\n");
  printf("       --rttmsg    or  -T       send time stamp as ASCII string data\n");
  printf("                                (reliable with high latencies, fails on noisy lines)\n");
  printf("       --txpattern or  -p       specifies the data pattern to send (default is fox message)\n");
  printf("       --rxpattern or  -r       specifies the data pattern to search (disables rttmsg mode)\n");
  printf("                                (default: same as TX pattern)\n");
  printf("       --rxlength  or  -b       specifies the RX pattern look back buffer length\n");
  printf("                                (default: length of RX pattern, max: %d)\n", CONST_PATTERNSTR);
  printf("\n");
  printf("  [*]  indicates parameter is required\n");
  printf("\n");
}

typedef enum
{
  ARG_D               =   0x0001,
  ARG_C               =   0x0002,
  ARG_I               =   0x0004,
  ARG_P               =   0x0008,
  ARG_R               =   0x0010,
  ARG_T               =   0x0020,
  ARG_B               =   0x0040,
  ARG_N               =   0x0080,
  ARG_TT              =   0x0100,
  ARG_PP              =   0x0200,
  ARG_AUTO_PARAMTEST  =   0x2000,
  ARG_AUTO_BAUDTEST   =   0x4000,
  ARG_HELP            =   0x8000,
} ARG_CONSTANTS;

/* If requiredArgs != 0 after parameter parsing, then we are missing  */
/* some absolutely necessary parameters.                              */
uint32_t requiredArgs = ARG_D | ARG_C;

uint32_t pingInterval = 1000000000; /* in nanoseconds, default 1.0s */
uint32_t dataTimeout_us = 0;
char devNameStr[CONST_DEVNAMESTR];
char configStr[CONST_CONFIGSTR];
char txPatternStr[CONST_PATTERNSTR] = "The quick brown fox jumped over the lazy dog 9876543210 times. ";
uint32_t txPatternLength = 0;
char rxPatternStr[CONST_PATTERNSTR];
uint32_t rxPatternLength = 0;
uint32_t rxBufferLength = 0;
char rxBuffer[CONST_PATTERNSTR * 2 + 1];    // need a double buffer, to look back into past inside the ring buffer
unsigned rxIdx = 0;
unsigned rxFill = 0;
bool rxTimeout = false;
int pingCount = -1;
bool rttmsgMode = false;
bool printTx = false;

#define min(a, b) (a <= b ? a : b)

/* Signal handler for clean exits. */
void onSigExit(int signal)
{
    keepRunning = false;
    pingCount = 0;
    dataTimeout_us = dataTimeout_us < 500000 ? 500000 : dataTimeout_us;

    (void) signal;  // eat up unused argument
}

unsigned countErrorChars(const char* str1, unsigned str1Len, const char* str2, unsigned str2Len)
{
  unsigned errChars = 0;

  errChars = abs(str1Len - str2Len);  // length difference are errors

  unsigned compLen = min(str1Len, str2Len);

  for (unsigned idx = 0; idx < compLen; ++idx)
  {
    if (str1[idx] != str2[idx])
      ++errChars;
  }

  return errChars;
}

unsigned getMatchQuality(const char* str1, unsigned str1Len, const char* str2, unsigned str2Len)
{
  unsigned qual = 0;

  unsigned compLen = min(str1Len, str2Len);
  
  if (compLen > 0)
    --compLen;

  for (unsigned idx = 0; idx < compLen; ++idx)
  {
    if (str1[idx] == str2[idx])
      ++qual;
    if (str1[idx+1] == str2[idx+1])
      ++qual;
  }

  return qual;
}

struct MatchResult
{
  unsigned backShift;
  unsigned testLength;
  unsigned charErrors;
};

struct MatchResult findClosestMatch()
{
  struct MatchResult matchResult;

  unsigned matchQuality = 0;
  matchResult.charErrors = rxPatternLength;
  matchResult.backShift = 0;
  matchResult.testLength = min(rxFill, rxPatternLength);

  if (rxFill >= rxPatternLength)
  {
    for (unsigned idx = 0; idx <= (rxFill - matchResult.testLength); ++idx)
    {
      int compareOffset = rxIdx - rxPatternLength - idx;
      if (compareOffset < 0)
        compareOffset += rxBufferLength;
      unsigned errLvl = countErrorChars(rxBuffer + compareOffset, matchResult.testLength, rxPatternStr, rxPatternLength);
      unsigned qual = getMatchQuality(rxBuffer + compareOffset, matchResult.testLength, rxPatternStr, rxPatternLength);
      if ((matchResult.charErrors > errLvl) || (matchQuality < qual))
      {
        matchResult.backShift = idx;
        matchResult.charErrors = errLvl;
        matchQuality = qual;
      }
    }
  }

  return matchResult;
}

void* txRoutine(void* arg)
{
  int spFd = *((int*) arg);

  return NULL;
}

int main(int argc, char **argv)
{
  int exitVal = 0;

  bool forcePatternMode = false;
  const char *optionString = "hd:c:i:t:p:r:b:n:TP";
  struct option optionTable[] =
  {
      { "help",           no_argument,        NULL, ARG_HELP },
      { "device",         required_argument,  NULL, ARG_D },
      { "config",         required_argument,  NULL, ARG_C },
      { "interval",       optional_argument,  NULL, ARG_I },
      { "timeout",        optional_argument,  NULL, ARG_T },
      { "count",          optional_argument,  NULL, ARG_N },
      { "rttmsg",         optional_argument,  NULL, ARG_TT },
      { "printtx",        optional_argument,  NULL, ARG_PP },
      { "txpattern",      optional_argument,  NULL, ARG_P },
      { "rxpattern",      optional_argument,  NULL, ARG_R },
      { "rxlength",       optional_argument,  NULL, ARG_B },
      { "auto_paramtest", no_argument,        NULL, ARG_AUTO_PARAMTEST },
      { "auto_baudtest",  no_argument,        NULL, ARG_AUTO_BAUDTEST  },
  };

  int optionIndex = 0;
  int getoptRet   = 0;

  memset(devNameStr, 0, sizeof(devNameStr));
  memset(configStr, 0, sizeof(configStr));

  memset(rxBuffer, ' ', sizeof(rxBuffer));
  txPatternLength = strlen(txPatternStr);
  strncpy(rxPatternStr, txPatternStr, (CONST_PATTERNSTR - 1));
  rxPatternLength = strlen(rxPatternStr);

  /* Process incoming options. */
  while(-1 != (getoptRet = getopt_long(argc, argv, optionString, optionTable, &optionIndex)))
  {
    /* printf("getoptRet: %c, %x\n", getoptRet, getoptRet); */
    switch(getoptRet)
    {
    case 'h':
    case ARG_HELP:
      Usage(argv[0]);
      return 0;
      break;
    case 'T':
    case ARG_TT:
      rttmsgMode = true;
      break;
    case 'P':
    case ARG_PP:
      printTx = true;
      break;
    case 'd':
    case ARG_D:
      if (strlen(optarg) >= CONST_DEVNAMESTR)
      {
        fprintf(stderr, "ERROR: Device name is too long.\n");
        goto cleanup_easy;
      }
      strncpy(devNameStr, optarg, (CONST_DEVNAMESTR - 1));
      requiredArgs &= ~(ARG_D);
      break;
    case 'c':
    case ARG_C:
      if (strlen(optarg) >= CONST_CONFIGSTR)
      {
        fprintf(stderr, "ERROR: Config string is too long.\n");
        goto cleanup_easy;
      }
      strncpy(configStr, optarg, (CONST_CONFIGSTR - 1));
      requiredArgs &= ~(ARG_C);
      break;
    case 'i':
    case ARG_I:
    {
      double pingInterval_d64 = atof(optarg);
      double maxInterval = (double) UINT32_MAX / 1000000.0;
      if (pingInterval_d64 > maxInterval)
      {
        fprintf(stderr, "ERROR: Cannot have an interval that large. Why would you want to?\n");
        goto cleanup_easy;
      }
      else
      {
        pingInterval = (uint32_t) (pingInterval_d64 * 1000000000.0);
      }
      requiredArgs &= ~(ARG_I);
      break;
    }
    case 'b':
    case ARG_B:
    {
      int blen = atoi(optarg);
      if (blen >= CONST_PATTERNSTR)
      {
        fprintf(stderr, "ERROR: RX buffer length to big. Why would you want to?\n");
        goto cleanup_easy;
      }
      else
      {
        rxBufferLength = blen;
      }
      requiredArgs &= ~(ARG_I);
      break;
    }
    case 't':
    case ARG_T:
    {
      double timeout_d64 = atof(optarg);
      double maxInterval = (double) UINT32_MAX / 1000000.0;
      if (timeout_d64 > maxInterval)
      {
        fprintf(stderr, "ERROR: Cannot have an interval that large. Why would you want to?\n");
        goto cleanup_easy;
      }
      else
      {
        dataTimeout_us = (uint32_t) (timeout_d64 * 1000000 + 0.5);
      }
      requiredArgs &= ~(ARG_T);
      break;
    }
    case 'n':
    case ARG_N:
    {
      uint32_t count = atoi(optarg);
      if (count >= UINT16_MAX)
      {
        fprintf(stderr, "ERROR: Ping count to large. Why would you want to?\n");
        goto cleanup_easy;
      }
      else
      {
        pingCount = count;
      }
      requiredArgs &= ~(ARG_N);
      break;
    }
    case 'p':
    case ARG_P:
      if (strlen(optarg) >= CONST_PATTERNSTR)
      {
        fprintf(stderr, "ERROR: TX pattern string is too long.\n");
        goto cleanup_easy;
      }
      strncpy(txPatternStr, optarg, (CONST_PATTERNSTR - 1));
      txPatternLength = strlen(txPatternStr);
      forcePatternMode = true;
      requiredArgs &= ~(ARG_P);
      break;
    case 'r':
    case ARG_R:
      if (strlen(optarg) >= CONST_PATTERNSTR)
      {
        fprintf(stderr, "ERROR: RX pattern string is too long.\n");
        goto cleanup_easy;
      }
      strncpy(rxPatternStr, optarg, (CONST_PATTERNSTR - 1));
      rxPatternLength = strlen(rxPatternStr);
      forcePatternMode = true;
      requiredArgs &= ~(ARG_R);
      break;
    case ARG_AUTO_PARAMTEST:
      break;
    case ARG_AUTO_BAUDTEST:
      break;
    default:
      Usage(argv[0]);
      goto cleanup_easy;
      break;
    }
  }

  /* If -c or -d was not specified, then throw an error here. */
  if (requiredArgs)
  {
    Usage(argv[0]);
    goto cleanup_easy;
  }

  if (rttmsgMode)
    rxBufferLength = CONST_PATTERNSTR;
  else if (rxBufferLength < rxPatternLength)
    rxBufferLength = rxPatternLength;

  if (forcePatternMode)
    rttmsgMode = false;

  /* Settings for a raw terminal. */
  struct termios ts;
  memset(&ts, 0, sizeof(ts));

  ts.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
  ts.c_oflag &= ~OPOST;
  ts.c_cflag &= ~(PARENB);
  ts.c_cflag |= CLOCAL;           /* Ignore modem control lines. */
  ts.c_cflag |= CREAD;            /* working read-write.   */
  ts.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  ts.c_cc[VMIN] = 0;
  ts.c_cc[VTIME] = 0;

  /* Parse config string. */
  char * underscorePtr = strchr(configStr, '_');

  if (NULL == underscorePtr)
  {
    fprintf(stderr, "ERROR: Character Size, Parity, and Stop Bit length not specified.\n");
    goto cleanup_easy;
  }

  if (4 != strlen(underscorePtr))
  {
    fprintf(stderr, "ERROR: Character Size, Parity, and Stop Bit length improperly specified.\n");
    goto cleanup_easy;
  }

  uint32_t baudRate = strtol(configStr, (char **) NULL, 10);
  const unsigned toChars = 10;
  switch(baudRate)
  {
  case 50:
    baudRate = B50;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 75:
    baudRate = B75;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 110:
    baudRate = B110;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 134:
    baudRate = B134;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 150:
    baudRate = B150;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 200:
    baudRate = B200;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 300:
    baudRate = B300;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 600:
    baudRate = B600;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 1200:
    baudRate = B1200;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 2400:
    baudRate = B2400;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 4800:
    baudRate = B4800;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 9600:
    baudRate = B9600;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 19200:
    baudRate = B19200;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 38400:
    baudRate = B38400;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 57600:
    baudRate = B57600;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
  case 115200:
    baudRate = B115200;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
    /* These two odd ones exist on cygwin. */
#ifdef  B128000
  case 128000:
    baudRate = B128000;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
#endif
  case 230400:
    baudRate = B230400;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
#ifdef  B256000
  case 256000:
    baudRate = B256000;
    if (!dataTimeout_us)
      dataTimeout_us = 12 * toChars * 1000000/baudRate + 500000/baudRate;
    break;
#endif
  default:
    fprintf(stderr, "ERROR: Invalid Baud Rate specified.\n");
    baudRate = B0;
    goto cleanup_easy;
    break;
  }
  ts.c_cflag &= ~(CBAUD);
  ts.c_cflag |= baudRate;

  uint32_t charSize;
  ++underscorePtr;
  switch(*underscorePtr)
  {
  case '5':
    charSize = CS5;
    break;
  case '6':
    charSize = CS6;
    break;
  case '7':
    charSize = CS7;
    break;
  case '8':
    charSize = CS8;
    break;
  default:
    fprintf(stderr, "ERROR: Invalid Character Size specified.\n");
    goto cleanup_easy;
    break;
  }
  ts.c_cflag &= ~(CSIZE);
  ts.c_cflag |= charSize;

  ++underscorePtr;
  switch(*underscorePtr)
  {
  case 'N':
    ts.c_cflag &= ~(PARENB);
    break;
  case 'E':
    ts.c_cflag |= PARENB;
    ts.c_cflag &= ~(PARODD);
    break;
  case 'O':
    ts.c_cflag |= PARENB;
    ts.c_cflag |= PARODD;
    break;
  default:
    fprintf(stderr, "ERROR: Invalid Parity specified.\n");
    goto cleanup_easy;
    break;
  }

  ++underscorePtr;
  switch(*underscorePtr)
  {
  case '1':
    ts.c_cflag &= ~(CSTOPB);
    break;
  case '2':
    ts.c_cflag |= CSTOPB;
    break;
  default:
    fprintf(stderr, "ERROR: Invalid number of Stop Bits specified.\n");
    goto cleanup_easy;
    break;
  }


  if (0 != access(devNameStr, W_OK | R_OK))
  {
    fprintf(stderr, "ERROR: User does not have permission to read or write data on: %s\n",
        devNameStr);
    goto cleanup_easy;
  }

  /* Open the serial port. */
  int spFd = open(devNameStr, O_RDWR | O_NONBLOCK | O_NOCTTY);
  if (-1 == spFd)
  {
    fprintf(stderr, "ERROR: open() call failed.\n");
    goto cleanup_easy;
  }

  /* Set the device options */
  if (0 != tcsetattr(spFd, TCSANOW, &ts))
  {
    perror("tcsetattr");
    goto cleanup_open;
  }

  /* Register signal handler. */
  signal(SIGINT,  onSigExit);
  signal(SIGTERM, onSigExit);

  int retval;

  /* Keep track of wall time. */
  struct timespec rxTime, next, tdiff, txTime;
  unsigned answerIdx;
  unsigned answerCount = 1; // fake init for getting the first timestamp
  uint64_t txSeq = 0, rxSeq = 0;
  unsigned timeoutCount_us;

  /* Start regular pings.   */
  clock_gettime(CLOCK_BOOTTIME, &next);
  while (keepRunning)
  {
    if (pingCount)
    {
      if (pingCount > 0)
      --pingCount;

      if (pingCount == 0)
        dataTimeout_us = dataTimeout_us < 500000 ? 500000 : dataTimeout_us;

      ++txSeq;

      clock_gettime(CLOCK_BOOTTIME, &rxTime);
      if (rttmsgMode)
      {
        int cnt = snprintf(txPatternStr, sizeof(txPatternStr), " T%09" PRIu64 ".%06" PRIu32 "s%04" PRIu64 "P ", (uint64_t) rxTime.tv_sec, (uint32_t) rxTime.tv_nsec / 1000, txSeq); // create some timestamping inside the payload, if desirable, else make a fifo of timestamps, drain it with every set of rxDataLength
        txPatternLength = cnt;
        rxPatternLength = cnt;
        txTime = rxTime;
        strncpy(rxPatternStr, txPatternStr, sizeof(rxPatternStr));
      }
      else if (answerCount > 0)  // else keep old time stamp, until we got something
        txTime = rxTime;

      if (!printTx && (1 == txSeq))
        printf("Transmitting packets with %u bytes of data.\n", txPatternLength);

      if (0 > write(spFd, txPatternStr, txPatternLength))
      {
        if (errno != EAGAIN)
        {
          perror("write");
          keepRunning = false;
          goto cleanup_open;
        }
      }
      else if (printTx)
      {
        printf("Sent: % 8d.%06d s, %u bytes, '%*.*s'\n", (int) rxTime.tv_sec, (int) rxTime.tv_nsec / 1000, txPatternLength, txPatternLength, txPatternLength, txPatternStr);
      }
    }
                                                                   
    answerIdx = rxIdx;
    answerCount = 0;  // we got anything yet
    timeoutCount_us = 0;

    do
    {
      // wait for input
      retval = read(spFd, rxBuffer + rxIdx, min(rxBufferLength - answerCount, rxPatternLength));

      // make ring buffer comparision easier by copying around the overhang
      if (retval > 0)
      {
        // take timestamp of the last input byte
        clock_gettime(CLOCK_BOOTTIME, &rxTime);

//        fprintf(stderr, "read: '%*.*s'\n", retval, retval, rxBuffer + rxIdx);

        // copy input to buffer overhang for later comparison
        if ((rxIdx + retval) < rxBufferLength)
        {
          // read data does not overhang, simple copy forward
          memcpy(rxBuffer + rxBufferLength + rxIdx, rxBuffer + rxIdx, retval);
        }
        else
        {
          // read data overhangs, copy overhang backward
          memcpy(rxBuffer, rxBuffer + rxBufferLength, retval - (rxBufferLength - rxIdx));
        }

        // adjust ring buffer index
        rxIdx += retval;
        rxIdx %= rxBufferLength;

        // adjust ring buffer fill
        if (rxFill < rxBufferLength)
        { // buffer not full yet
          if ((rxFill + retval) < rxBufferLength)
            rxFill += retval; // still filling
          else
            rxFill = rxBufferLength; // now full
        }

        answerCount += retval;
        timeoutCount_us = 0;
        rxTimeout = false;

        if (rttmsgMode)
        {
          uint64_t sec = 0, usec = 0;
          int cnt = 0;
          char cT[2],cdot[2],cs[2],cP[2];
          uint64_t seq;

          rxSeq = 0;

          // try to find the latest
          do
          {
            char * lastT = memrchr(rxBuffer + answerIdx, 'T', answerCount);
            if (lastT)
            {
              rxBuffer[answerIdx + answerCount] = 0; // terminate the input for scanning
//              fprintf(stderr, "to scan 1: '%s'\n", lastT);
              cnt = sscanf(lastT, "%1[T]%" SCNu64 "%1[.]%" SCNu64 "%1[s]%" SCNu64 "%1[P] ", cT, &sec, cdot, &usec, cs, &seq, cP);
//              fprintf(stderr, "1 cnt = %i, toCnt = %u us, answerCount = %u\n", cnt, timeoutCount_us, answerCount);

              if (cnt == 7)
                break;

              // ok, last String is not complete, give one more try
              lastT = memrchr(rxBuffer + answerIdx, 'T', lastT - (rxBuffer + answerIdx));
              if (lastT)
              {
//                fprintf(stderr, "to scan 2: '%s'\n", lastT);
                cnt = sscanf(lastT, "%1[T]%" SCNu64 "%1[.]%" SCNu64 "%1[s]%" SCNu64 "%1[P] ", cT, &sec, cdot, &usec, cs, &seq, cP);
//                fprintf(stderr, "2 cnt = %i\n", cnt, timeoutCount_us, answerCount);
  //
  //              if (cnt == 7)
  //                break;
              }
            }
          } while (0);

          if (cnt == 7)
          {
//            fprintf(stderr, "3 cnt = %i, toCnt = %u us, answerCount = %u\n", cnt, timeoutCount_us, answerCount);
            txTime.tv_sec = sec;
            txTime.tv_nsec = usec * 1000;
            rxSeq = seq;
            break;
          }
        }
      }
      else
      {
        if (retval < 0)
        {
          if (errno == EAGAIN)
          {
            retval = 0;
          }
          else
          {
            perror("read");
            keepRunning = false;
            goto cleanup_open;
          }

        }

        usleep(10);
        timeoutCount_us += 10;

        if (timeoutCount_us > dataTimeout_us)
        {
          rxTimeout = true;
          break;
        }
      }

      if (!rttmsgMode && (answerCount >= rxPatternLength))  // collected enough characters for searching the rxPattern
        break;

    } while (0 <= retval);

    if (answerCount > 0)
    {
      tdiff.tv_sec = rxTime.tv_sec - txTime.tv_sec;

      if (txTime.tv_nsec > rxTime.tv_nsec)
      {
        rxTime.tv_nsec += 1000000000;
        if (tdiff.tv_sec > 0)
          tdiff.tv_sec -= 1;
      }

      tdiff.tv_nsec = rxTime.tv_nsec - txTime.tv_nsec;

      // adjust for character timeout of last character
      if (tdiff.tv_nsec < timeoutCount_us) // is smaller than ping timeout
      {
        tdiff.tv_nsec += 1000000000;
        if (tdiff.tv_sec > 0)
          tdiff.tv_sec -= 1;
      }
      tdiff.tv_nsec -= timeoutCount_us; // deduct ping timeout

      if (!rxTimeout)
      {
        if (!rttmsgMode)
        {
          const char fmt1[] = " RTT: % 8d.%06d s, %3u bytes, '%*.*s'\n";
          const char fmt2[] = "Recv: %+8d.%06d s, %3u bytes, '%*.*s'\n";
          printf(printTx ? fmt2 : fmt1, (int) tdiff.tv_sec, (int) tdiff.tv_nsec / 1000, answerCount, answerCount, answerCount, rxBuffer + answerIdx);

          struct MatchResult matchResult = findClosestMatch();

          printf("Matching characters: %d, errors: %d, back shift: %d\n", matchResult.testLength - matchResult.charErrors, matchResult.charErrors, matchResult.backShift);
        }
        else
        {
          const char fmt1[] = " RTT: % 8d.%06d s, %3u bytes, seq=%" PRIi64 "\n";
          const char fmt2[] = "Recv: %+8d.%06d s, %3u bytes, seq=%" PRIi64 "\n";
          printf(printTx ? fmt2 : fmt1, (int) tdiff.tv_sec, (int) tdiff.tv_nsec / 1000, answerCount, rxSeq);
        }
      }
      else
      {
        if (!rttmsgMode)
        {
          const char fmt1[] = " RTT:         (timeout), %3u bytes, '%*.*s'\n";
          const char fmt2[] = "Recv:         (timeout), %3u bytes, '%*.*s'\n";
          printf(printTx ? fmt2 : fmt1, answerCount, answerCount, answerCount, rxBuffer + answerIdx);

          struct MatchResult matchResult = findClosestMatch();

          printf("Matching characters: %d, errors: %d, back shift: %d\n", matchResult.testLength - matchResult.charErrors, matchResult.charErrors, matchResult.backShift);
        }
        else
        {
          const char fmt1[] = " RTT:         (timeout), %3u bytes\n";
          const char fmt2[] = "Recv:         (timeout), %3u bytes\n";
          printf(printTx ? fmt2 : fmt1, answerCount);
        }
      }

      fflush(stdout);

      // TODO: print difference marker
    }

    // TODO: drain RX device into our RX buffer

    next.tv_nsec += pingInterval;
    next.tv_sec += next.tv_nsec / 1000000000;
    next.tv_nsec %= 1000000000;
    while ((retval = clock_nanosleep(CLOCK_BOOTTIME, TIMER_ABSTIME, &next, NULL)))  // assign by intent
    {
      if (!(retval == EINTR))
      {
        keepRunning = false;
        break;
      }
    }

    if ((0 == pingCount) && rxTimeout)
    {
      keepRunning = false;
      break;
    }
  }

  close(spFd);
  return exitVal;

  /* C "exception" unwinders. */
  cleanup_open:
  close(spFd);
  cleanup_easy:
  return -1;
}

