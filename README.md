# serialrtt

## Description

Original idea stems from https://vilimpoc.org/research/serialping/, developed somewhat further to get some round-trip time measurements on serial ports, connected with loopback connector or something, that responds with an known message.

## Installation

* create a `build` directory, run `cmake` there
* run `make` in the build directory
* now the program is ready to use, try `./serialrtt --help` or `./serialping --help`

## Usage

### serialping

This is mostly the orignal program from Max Vilimpoc, with cosmetic minor modifications.

It just sends some TX pattern data to the serial line and prints the timestamp on STDOUT.

Try to get help for it by calling `serialping --help`.

### serialrtt

This is based on `serialping`, but targets at the measurement of round-trip-times (RTT).

It sends some TX data pattern to the serial line, prints the timestamp and the data pattern on STDOUT 
and then waits for some time for an answer. 

The default settings expect the same string back, the program looks for as much simelarity as possible 
by inspecting the RX buffer. The default size of the RX data buffer is the length of the RX data pattern,
which defaults to the TX data pattern.

The default TX data pattern is the 63 bytes long fox message:
   `'The quick brown fox jumped over the lazy dog 9876543210 times. '`

There is a second mode available, called rttmsg mode, where the current timestamp and a sequence number 
is written to the serial line. As soon as the message returns, it is tried to scan it and a time delta 
is calculated from the this. If there are multiple messages in the RX buffer, then the last one, which 
is complete, is evaluated. Therefor the reported sequence number might skip some counts in this case.

Try to get some idea about the usage by calling `serialrtt --help`.

## Contribution

Suggestions are welcome.

## Credits

The code is based on the work from Max Vilimpoc, please see https://vilimpoc.org/research/serialping/ 

## License

    Copyright (c) 2008, Max Vilimpoc, http://vilimpoc.org/
    Copyright (c) 2022, Johannes Lode, https://mtronig.de/
    
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    
        * Redistributions of source code must retain the above
          copyright notice, this list of conditions and the following
          disclaimer.
        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials
          provided with the distribution.
        * Neither the name of the author nor the names of its
          contributors may be used to endorse or promote products
          derived from this software without specific prior written
          permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

